<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ohc' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'tR:F20*a]uFG{J]}H4>b}LlmwR2}/SyvQ]>ZnzP[kaW|<tKGOgfW~IRt |Jn<y(.' );
define( 'SECURE_AUTH_KEY',  'NaWZ/Adh9?h8R^bI~9[8VbmEI-K$,<:[|nSqq-vaQ:_>%X3s2I%I~L{=p^*dh/}w' );
define( 'LOGGED_IN_KEY',    '{enFhD65XF7;a;H3{rP~y{:LgL3!EojH^NCfLI.U)#xnfjB&I_a;[@|*RFi>QB$%' );
define( 'NONCE_KEY',        'Z.e}0m+FrMegtT+hoALB?j#U0W_(Ssz2md%lx7?r1UMwrtaioetRM]!J(p/wGEl;' );
define( 'AUTH_SALT',        'n;uHtnMeD#7@Cm0<qtIV5Ic],Nw+;t#q}J`9zfRxlIZE^s[.Sal![*^i.h3}gJ)Q' );
define( 'SECURE_AUTH_SALT', 's|Dj%0 BFNm2(9>Rt1J<8Na;m<P[2_nRM5.3pvYfig55fsp^$^*OO7ER[hHTFHMC' );
define( 'LOGGED_IN_SALT',   'O*|6S3mJb-Y1]OA1u:OKrtPs6~%nAhI*,.Z|^_wz&+6B*W_#e:I*EI&Bu,jKpr_+' );
define( 'NONCE_SALT',       'b!0T8`+h*Nx3$R@a@hn^rg(Jp?xx_b}M$WQ,&lQ(6~?/B*A+w/c^7kLe}V{L,Z53' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
